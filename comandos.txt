# Configurações Iniciais para utilização do git
# git config -- global user.name 
# git config -- global user.email
# git config -- global core.editor
#
#Exemplos 
# git config -- global user.name "Alex Gonçalves"
# git config -- global user.e-mail "asg.goncalves@gmail.com"
# git config -- global core.editor "vim"
#
# # Aprendendo mais sobre os comandos do Git # #
#
# 1 - git --version (Exibe a versão do git instalado)
#
# 2 - git init (Informa para o git que determinado diretorio/arquivo comecará a ser rastreado/trackeado pelo git, cria-se um arquivo .git dentro do diretório corrente)
#
# 3 - git status (Verifica se há arquivos que foram modificados local ou remoto e exibe na tela)
#
# 4 - git clone (Comando clona/baixa para local remoto os diretorios/arquivos/projetos/etc/ podendo ser via ssh ou https)
# Sintaxe = git clone + endereço remoto
# Exemplo =  git clone https://gitlab.com/devops-expert-linuxtips/descomplicando-o-gitlab.git 
#
# 5 - git add (Comando inicia o rastramento dos arquivos/diretorios/etc)
Sintaxe = git add + nome dos arquivos/diretorios
Exemplo = git add comandos.txt ou git add pasta-imagens
#
# 6 - git commit (Define uma pacote de arquivos que foram modificados e precisam ser publicados, trabalha com a opçãp -m "texto" informando resumidamente qual a finalidade daquele commit
Sintaxe = git commit -m "Mensagem" + nome do arquivo/diretorio
Exemplo = git commit -m "Commit Initital" comandos.txt ou git commit -m "Adicionando o primeiro arquivo na branch master" comandos.txt 
#
# 7 - git push (Publica o commit na master por default -> fluxo from local to remoto)
#
# 8 - git push origin giropops (Colocamos como parâmetro para o comando git push em qual branch será publicada as alterações/inserções/etc/)
# Sintaxe =  git push origin branch-secundaria nome do arquivo/diretorio
# Exemplo = git push origin branch-secundaria comandos.txt
#
# 9 - git log (Exibe todos os logs com todos os detalhes)
# 
# 10 - git log -3 (Exibe os 3 últimos logs)
#
# 11 -  git log outline (Exibe os logs de forma simplificada)
#
# 12 - git log --graph --decorate (Exibe os logs com as informações das ramificações/árvores)
#
# 13 - git log --author (Exibe os logs conforme os parâmetros do author/criador/desenvolvedor/etc/)
# Exemplo = git log --author="Alex Gonalves"
#
# 14 - git pull (Inverso do git push o git pull compara as modificações feitas no servidor remoto/repositório e atualiza os diretorios/arquivos locais -> fluxo from remoto to local)
#
# 15 -  git rm (Remove um arquivo | Após utilizar este comando necessário rodar o git commit -m "Mensagem" informando a modificação e o git push)
# Sintaxe = git rm + nome do arquivo
# Exemplo = git rm comandos.txt
#
# 16 - git checkout -b (Comando cria uma branch e altera para ela automaticamente)
# Sintaxe =  git checkout -b branch-secundaria + nome do arquivo
# Exemplo = git checkout -b branch-secundaria comandos.txt
#
# git remote add nome do servidor remoto origin git@gitlab.com:badtux_/testando.git
   
CRIADO UMA NOVA BRANCH COMO COMANDO git flow feature start nome-da-feature
Ex -> git flow feature start criar-feature




==================================================================================================================================================================================================================

# Criado o novo arquivo giropops e inserido algumas informações para fazer o commit e depois executar o comando git flow feature finish nome-da-feature

# Adicionandos alguns comandos do git flow

git apt install gitflow (Instala o git gitflow)
git init (Comando idêntico par ao git e git flow para iniciar o git)
git branch --list (Comando lista as branchs do projeto e informa em qual branch corrente)
git flow feature start nome-da-feature (Comando para criar uma ramificação feature e iniciá-la automaticamente e alternando para ela)
# Exemplo -> git flow feature start app-criar-botao-follow
#
git flow feature publish nome-da-feature (Comando para publicar as modificações e exibi-las para outros integrantes que possam estar trabalhando no mesmo projeto)
# Exemplo -> git flow feature publish app-criar-botao-follow
#
# git flow feature finish nome-da-feature (Comando finiliza a branch e faz um merge com a branch principal)
# Exemplo -> git flow feature finish app-criar-botao-follow
#
# git flow release start versão-da-release (Comando para criar uma ramificação de release e iniciá-la automaticamente e alternando pare ela)
# Exemplo -> git flow release start versao1.0
git flow release publish versão-da-release (Comando segue a mesma ideia do feature) 
git flow release finish versão-da-release (Comando segue a mesma ideia do feature)
#
# git flow hotflix start nome-do-hotfix (Comando para criar uma ramificação de hotfix e iniciá-la automaticamente e alternando para ela)
# Exemplo -> git flow hotfix start correcao-api-gateways-de-pagamentos
git flow hotfix publish nome-do-hotfix (verificar se esta opção existe) (Existindo segue a mesma ideia do feature e release)
git flow hotfix finish nome-do-hotfix (Segue a mesma ideia do feature e release)
#
Obs: As modificações até este momento estão localmente sendo necessário fazer o git push para atualizar o repositório remoto
git push origin nome-da-branch ou git push --all (publica no repositório em todas as branchs, desta forma mantendo apenas a branch principal)

==================================================================================================================================================================================================================

